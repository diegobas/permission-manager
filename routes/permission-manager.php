<?php

use DiegoBas\PermissionManager\App\Http\Middleware\LocalizationMiddleware;

/*
|--------------------------------------------------------------------------
| DiegoBas\PermissionManager Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the DiegoBas\PermissionManager package.
|
*/
Route::group([
	'namespace' => 'DiegoBas\PermissionManager\App\Http\Controllers\Web',
	'prefix' => '',
    'middleware' => ['web', LocalizationMiddleware::class]
], function () {
    Route::get('/lang/{locale}', 'WebController@setLocale')->name('lang');
});

Route::group([
    'namespace'  => 'DiegoBas\PermissionManager\App\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(), LocalizationMiddleware::class],
], function () {
	//Localization
	Route::get('setlocale/{locale}', 'BaseController@setLocale');

    //Dashboard
    Route::get('dashboard', 'PermissionManagerAdminController@dashboard')->name('permission-manager.dashboard');
    Route::get('/', 'PermissionManagerAdminController@redirect')->name('permission-manager');
    
    //Permision manager
    Route::group ([
        'namespace'  => 'Administration',
    ], function() {
	   
        Route::crud('permission', 'PermissionCrudController');
        Route::crud('role', 'RoleCrudController');
        Route::crud('user', 'UserCrudController');
    });
});