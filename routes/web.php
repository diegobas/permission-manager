<?php

use DiegoBas\PermissionManager\App\Http\Middleware\LocalizationMiddleware;

/*
|--------------------------------------------------------------------------
| DiegoBas\PermissionManager Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the DiegoBas\PermissionManager package.
|
*/
Route::group([
	'namespace' 	=> 'DiegoBas\PermissionManager\App\Http\Controllers\Web',
	'prefix' 		=> '',
    'middleware' 	=> ['web', LocalizationMiddleware::class]
], function () {
    Route::get('/lang/{locale}', 'WebController@setLocale')->name('lang');
});