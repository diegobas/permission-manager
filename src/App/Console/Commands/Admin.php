<?php

namespace DiegoBas\PermissionManager\App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use DiegoBas\PermissionManager\App\Models\User;
use DiegoBas\PermissionManager\App\Console\Commands\CommonCommands;

class Admin extends CommonCommands
{
    protected $progressBar;
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission-manager:admin {--O|option=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Administration utility for Permission Manager.';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$option = $this->option('option');
    	if (!$option) {
    		$this->error('--O|option no set.');
    		$this->help();
    		return;
    	}

    	switch ($option) {
            case 'user':
                $this->createUser();
                break;

    	 	case 'role':
    	 		$this->assignRole();
    	 		break;
    	 	
    	 	default:
    	 		$this->error('Option not recognized!');
    	 		$this->help();
    	 		break;
    	 }
    }

    private function help()
    {
    	$this->info('Valid option values are:
        user : Create new administrator user.
    	role : Assign specified role to a given user.');
    }

    public function createUser()
    {
        $this->info('Creating a new user');

        $name = $this->ask('Name');
        
        $exit = false;
        while(!$exit) {
            $email = $this->ask('Email');
            $user = User::where('email', $email)->first();
            if (!$user) {
                $exit = true;
            } else {
                $this->error('The email already exists on the database.');
            }
        }

        $password = $this->secret('Password');
        $password = bcrypt($password);

        $auth = 'DiegoBas\PermissionManager\App\Models\User';
        $user = new $auth();
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;

        try {
            if ($user->save()) {
                $this->info('Successfully created new user');            
                $this->assignRole($email);
            } else {
                $this->error('Something went wrong trying to save your user');
            }
        } catch (Illuminate\Database\QueryException $e) {
            $this->error('Something went wrong saving the user to database.');
        } catch(Exception $e) {
            $this->error('Something went wrong trying to save your user');
        }
    }

    private function assignRole($email =  null)
    {
    	$exit = false;
        while (!$exit) {
            $user_role = $this->ask('Role to assign');
            if ($user_role == null) {
                $user_role = '';
            }

        	try {
        		$role = Role::findByName($user_role, 'backpack');	
                $exit = true;    		
        	} catch (\Spatie\Permission\Exceptions\RoleDoesNotExist $e) {
        		$this->error('Role ' . $user_role . ' not found');
        		if ($create_role === 'Y') {
                    try {
                        $role = Role::create(['guard_name' => 'backpack', 'name' => $user_role]);
                        $exit = true;
                    } catch (Exception $e) {
                        $this->error('Error creating new role!');
                    }                    
                } else {
                    $this->info('Use one of this:');

                    $headers = ['name'];
                    $roles = Role::all(['name'])->toArray();
                    $this->table($headers, $roles);
                }
        	}
        }
    	
        $exit = false;
        while(!$exit) {
            if (!$email) {
        	    $email = $this->ask('User email');                
        	}

        	$user = User::where('email', $email)->first();

        	if ($user) {
        		try {
                    $user->guard_name = 'backpack';
        			$user->assignRole($role->name);
        			$this->line('Assigned ' . $role->name . ' role to ' . $user->name);
                    $exit = true;
        		} catch (Exception $e) {
        			$this->error('Error assigning roles');
                    return;
        		}    		
        	} else {
        		$this->error('Email not found in USERS table');
        		$this->info('Use one of this:');

        		$headers = ['name', 'email'];
        		$users = User::all(['name', 'email'])->toArray();
        		$this->table($headers, $users);
        	}
        }
    }
}