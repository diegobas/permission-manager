<?php

namespace DiegoBas\PermissionManager\App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CommonCommands extends Command
{

	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run a SSH command.
     *
     * @param string $command      The SSH command that needs to be run
     * @param bool   $beforeNotice Information for the user before the command is run
     * @param bool   $afterNotice  Information for the user after the command is run
     *
     * @return mixed Command-line output
     */
    public function executeProcess($command, $beforeNotice = false, $afterNotice = false, $isShellCommand = false)
    {
        $this->echo('info', $beforeNotice ? ' '.$beforeNotice : $command);

        //Workaround to Process updated to 5.x
        if (!$isShellCommand) {
            $commandArray = explode(" ", $command);
            $process = new Process($commandArray, null, null, null, 600);
        } else {
            $process = Process::fromShellCommandline($command);
        }

        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                $this->echo('comment', $buffer);
            } else {
                $this->echo('line', $buffer);
            }
        });

        // executes after the command finishes
        if (! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        if ($this->progressBar) {
            $this->progressBar->advance();
        }

        if ($afterNotice) {
            $this->echo('info', $afterNotice);
        }
    }

    /**
     * Write text to the screen for the user to see.
     *
     * @param string $type    line, info, comment, question, error
     * @param string $content
     */
    public function echo($type, $content)
    {
        // skip empty lines
        if (trim($content)) {
            $this->{$type}($content);
        }
    }
}