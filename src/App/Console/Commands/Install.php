<?php

namespace DiegoBas\PermissionManager\App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Exceptions\RoleDoesNotExist;

use DiegoBas\PermissionManager\App\Console\Commands\CommonCommands;

class Install extends CommonCommands
{
    protected $progressBar;
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission-manager:install';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs the Permission Manager module for Laravel BackPack.';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       	$this->progressBar = $this->output->createProgressBar(4);
        $this->progressBar->minSecondsBetweenRedraws(0);
        $this->progressBar->maxSecondsBetweenRedraws(120);
        $this->progressBar->setRedrawFrequency(1);

        $this->progressBar->start();

        $this->line(' Permission Manager installation started. Please wait...');
        $this->progressBar->advance();

        $this->line(" Generating needed tables (using Laravel's default migrations)");
        $this->executeProcess('php artisan migrate');

        $this->line(" Creating permissions");
        $this->createPermissions();
        $this->progressBar->advance();

        $this->progressBar->setRedrawFrequency(0);
        $this->line(' Adding sidebar menu item for Administration');
        switch (DIRECTORY_SEPARATOR) {
            case '/': // unix
                $this->executeProcess('php artisan backpack:add-sidebar-content "@canany([\'users\', \'user_groups\', \'user_permissions\'])<li class=\"nav-item nav-dropdown\"><a class=\"nav-link nav-dropdown-toggle\" href=\"#\"><i class=\"nav-icon la la-globe\"></i> {{ trans(\'permission-manager::base.administration\') }}</a><ul class=\"nav-dropdown-items\">@can(\'users\')<li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ backpack_url(\'user\') }}\"><i class=\"nav-icon la la-list\"></i> <span>{{ trans(\'permission-manager::base.users\') }}</span></a></li>@endcan @can(\'user_groups\')<li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ backpack_url(\'role\') }}\"><i class=\"nav-icon la la-cubes\"></i> <span>{{ trans(\'permission-manager::base.roles\') }}</span></a></li>@endcan @can(\'user_permissions\')<li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ backpack_url(\'permission\') }}\"><i class=\"nav-icon la la-cubes\"></i> <span>{{ trans(\'permission-manager::base.permissions\') }}</span></a></li>@endcan</ul></li>@endcan"', ' Adding menu options', 'Menu options added!', true);                
                break;
            case '\\': // windows
                $this->executeProcess('php artisan backpack:add-sidebar-content "@canany([\'users\', \'user_groups\', \'user_permissions\'])<li class=""nav-item nav-dropdown""><a class=""nav-link nav-dropdown-toggle"" href=""#""><i class=""nav-icon la la-globe""></i> {{ trans(\'permission-manager::base.administration\') }}</a><ul class=""nav-dropdown-items"">@can(\'users\')<li class=""nav-item""><a class=""nav-link"" href=""{{ backpack_url(\'user\') }}""><i class=""nav-icon la la-list""></i> <span>{{ trans(\'permission-manager::base.users\') }}</span></a></li>@endcan @can(\'user_groups\')<li class=""nav-item""><a class=""nav-link"" href=""{{ backpack_url(\'role\') }}""><i class=""nav-icon la la-cubes""></i> <span>{{ trans(\'permission-manager::base.roles\') }}</span></a></li>@endcan @can(\'user_permissions\')<li class=""nav-item""><a class=""nav-link"" href=""{{ backpack_url(\'permission\') }}""><i class=""nav-icon la la-cubes""></i> <span>{{ trans(\'permission-manager::base.permissions\') }}</span></a></li>@endcan</ul></li>@endcan"', ' Adding menu options', 'Menu options added!', true);
                break;
        }

        $this->progressBar->finish();
        $this->line(' Permission Manager installation finished.');
    }

    public function createPermissions()
    {
        $userPermission = Permission::findOrCreate('users', 'backpack');
        $this->echo('line', ' Created ' . $userPermission->name . ' permission.');

        $groupsPermission = Permission::findOrCreate('user_groups', 'backpack');
        $this->echo('line', ' Created ' . $groupsPermission->name . ' permission.');

        $permissionsPermission = Permission::findOrCreate('user_permissions', 'backpack');
        $this->echo('line', ' Created ' . $permissionsPermission->name . ' permission.');

        try {
            $superadminRole = Role::findByName('superadmin', 'backpack');
        } catch (RoleDoesNotExist $e) {
            $this->error(' Superadmin role does not exists!');
            $this->line(' Creating superadmin role...');
            $superadminRole = Role::create(['name' => 'superadmin', 'guard_name' => 'backpack']);
            $this->line(' Role created!');
        }

        if ($superadminRole) {
            $this->echo('line', ' Adding permissions to role ' . $superadminRole->name . '.');
            if ($userPermission) { 
                $superadminRole->givePermissionTo($userPermission); 
                $this->echo('line', ' Users access granted to role ' . $superadminRole->name . '.');
            }
            if ($groupsPermission) {
                $superadminRole->givePermissionTo($groupsPermission); 
                $this->echo('line', ' Groups access granted to role ' . $superadminRole->name . '.');
            }
            if ($permissionsPermission) { 
                $superadminRole->givePermissionTo($permissionsPermission); 
                $this->echo('line', ' Permissions access granted to role ' . $superadminRole->name . '.');
            }
            
        }
    }
}