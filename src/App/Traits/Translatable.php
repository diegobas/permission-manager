<?php

namespace DiegoBas\PermissionManager\App\Traits;

use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

trait Translatable
{
	use HasTranslations;

	public function getNullableTranslations(string $key = null)
	{
		$translations = $this->getTranslations($key);

		if (is_array($translations) && empty($translations)) {
			return null;
		}

		return $translations;
	}
}