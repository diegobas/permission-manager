<?php

namespace DiegoBas\PermissionManager\App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\Permission\Models\Role as OriginalRole;

use DiegoBas\PermissionManager\App\Models\Permission;

class Role extends OriginalRole
{
    use CrudTrait;

    protected $fillable = ['name', 'guard_name', 'updated_at', 'created_at'];   

    public function permissions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return parent::permissions()->withPivot('can_list', 'can_view', 'can_create', 'can_update', 'can_delete');
    }     
}
