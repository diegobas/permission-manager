<?php

namespace DiegoBas\PermissionManager\App\Models;

use App\Models\User as NativeUser;

use Spatie\Permission\Traits\HasRoles;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;

class User extends NativeUser
{
    use Notifiable;
    use CrudTrait;
    use HasRoles;


    protected $table = 'users';
    protected $guard_name = 'backpack';

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getIsSuperAdminAttribute()
    {
        return $this->isSuperAdmin();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeAllowed($query)
    {

        $loggedUserRoles = Auth::user()->roles->pluck('name')->toArray();
        $allowedRoles = [];

        if (in_array('superadmin', $loggedUserRoles)) {
            $allowedRoles = ['superadmin', 'admin', 'director', 'coordinator', 'teacher'];
        } else if (in_array('admin', $loggedUserRoles)) {
            $allowedRoles = ['admin', 'director', 'coordinator', 'teacher'];
        } else {
            return $query->where('id', null);
        }

        return $query->role($allowedRoles);        
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    public function isAdmin()
    {
        return $this->hasAnyRole(config('backpack.permissionmanager.models.role')::all());
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('superadmin');
    }
}
