<?php

namespace DiegoBas\PermissionManager\App\Http\Middleware;

use App;
use Closure;

class LocalizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('locale')) {
            App::setLocale(session('locale'));
        } else {
            App::setLocale('es');
            session(['locale' => 'es']);
        }

        return $next($request);
    }
}
