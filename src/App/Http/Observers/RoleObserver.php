<?php

namespace DiegoBas\PermissionManager\App\Http\Observers;

use DiegoBas\PermissionManager\App\Models\Role;

class RoleObserver
{
    public function creating(Role $role)
    {
        //dd($role);
    }

    public function updating(Role $role)
    {
       //dd($role);
    }

    public function deleting(Role $role) {
        if ($role->name == 'superadmin') {
            return false;
        }
    }
}