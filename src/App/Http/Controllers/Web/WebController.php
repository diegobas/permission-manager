<?php

namespace DiegoBas\PermissionManager\App\Http\Controllers\Web;

use App;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class WebController extends Controller
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public function setLocale($locale) 
    {
        if (array_key_exists ($locale, config('backpack.crud.locales'))) {
    		App::setLocale($locale);
    	    session(['locale' => $locale]);  
		}

    	return redirect()->back();
    }
}