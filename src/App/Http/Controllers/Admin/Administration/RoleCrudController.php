<?php

namespace DiegoBas\PermissionManager\App\Http\Controllers\Admin\Administration;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use DiegoBas\PermissionManager\App\Http\Requests\RoleStoreCrudRequest as StoreRequest;
use DiegoBas\PermissionManager\App\Http\Requests\RoleUpdateCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

// VALIDATION

class RoleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->role_model = $role_model = config('permission-manager.models.role');
        $this->permission_model = $permission_model = config('permission-manager.models.permission');

        $this->crud->setModel($role_model);
        $this->crud->setEntityNameStrings(trans('permission-manager::base.role'), trans('permission-manager::base.roles'));
        $this->crud->setRoute(backpack_url('role'));

        // deny access according to configuration file
        if (config('permission-manager.allow_role_create') == false) {
            $this->crud->denyAccess('create');
        }
        if (config('permission-manager.allow_role_update') == false) {
            $this->crud->denyAccess('update');
        }
        if (config('permission-manager.allow_role_delete') == false) {
            $this->crud->denyAccess('delete');
        }

        if (!Auth::user()->hasRole('superadmin')) {
            $this->crud->denyAccess('list');
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
        }
    }

    public function setupListOperation()
    {
        $this->crud->addColumn([
            'name'  => 'name',
            'label' => trans('permission-manager::base.name'),
            'type'  => 'text',
        ]);
        $this->crud->addColumn([   // select_multiple: n-n relationship (with pivot table)
            'label'     => trans('permission-manager::base.users'), // Table column heading
            'type'      => 'relationship_count',
            'name'      => 'users', // the method that defines the relationship in your Model
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('user?role='.$entry->getKey());
                },
            ],
            'suffix'    => ' users',
        ]);
        if (config('backpack.permissionmanager.multiple_guards')) {
            $this->crud->addColumn([
                'name'  => 'guard_name',
                'label' => trans('permission-manager::base.guard_type'),
                'type'  => 'text',
            ]);
        }
        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label'     => ucfirst(trans('permission-manager::base.permission_plural')),
            'type'      => 'select_multiple',
            'name'      => 'permissions', // the method that defines the relationship in your Model
            'entity'    => 'permissions', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => $this->permission_model, // foreign key model
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
        ]);
    }

    public function setupCreateOperation()
    {
        $this->addFields();
        $this->crud->setValidation(StoreRequest::class);

        //otherwise, changes won't have effect
        \Cache::forget('spatie.permission.cache');
    }

    public function setupUpdateOperation()
    {
        $this->addFields();
        $this->crud->setValidation(UpdateRequest::class);

        //otherwise, changes won't have effect
        \Cache::forget('spatie.permission.cache');
    }

    private function addFields()
    {
        $this->crud->addField([
            'name'  => 'name',
            'label' => trans('permission-manager::base.name'),
            'type'  => 'text',
        ]);

        if (config('permission-manager.multiple_guards')) {
            $this->crud->addField([
                'name'    => 'guard_name',
                'label'   => trans('permission-manager::base.guard_type'),
                'type'    => 'select_from_array',
                'options' => $this->getGuardTypes(),
            ]);
        }
        
        $this->crud->addField([
            'label'     => ucfirst(trans('permission-manager::base.permission_plural')),
            'type'      => 'permission',
            'name'      => 'permissions',
            'entity'    => 'permissions',
            'attribute' => 'name',
            'model'     => $this->permission_model,
            'pivot'     => false,
            'view_namespace'    => 'permission-manager::fields',
        ]);
    }

    /*
     * Get an array list of all available guard types
     * that have been defined in app/config/auth.php
     *
     * @return array
     **/
    private function getGuardTypes()
    {
        $guards = config('auth.guards');

        $returnable = [];
        foreach ($guards as $key => $details) {
            $returnable[$key] = $key;
        }

        return $returnable;
    }

    public function store() {
        $request = $this->crud->getRequest();
        $permissions = json_decode($request->input('permissions'), true);

        // do something before save
        $response = $this->traitStore();

        $this->assignPermissions($permissions);
        // do something after save
        return $response;
    }

    public function update() {
        $request = $this->crud->getRequest();
        $permissions = json_decode($request->input('permissions'), true);
        
        $this->assignPermissions($permissions);
        
        // do something before save
        $response = $this->traitUpdate();
        // do something after save
        return $response;
    }

    private function assignPermissions($permissions) {
        $grouped = [];
        foreach($permissions as $permission) {
            foreach($permission as $key => $value) {
                $grouped[$key] = $value;
            }
        }

        $role = $this->crud->getCurrentEntry();
        $role->permissions()->sync($grouped);        
    }
}
