<?php

namespace DiegoBas\PermissionManager\App\Http\Controllers\Admin\Administration;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use DiegoBas\PermissionManager\App\Http\Requests\PermissionStoreCrudRequest as StoreRequest;
use DiegoBas\PermissionManager\App\Http\Requests\PermissionUpdateCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

// VALIDATION

class PermissionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->role_model = $role_model = config('permission-manager.models.role');
        $this->permission_model = $permission_model = config('permission-manager.models.permission');

        $this->crud->setModel($permission_model);
        $this->crud->setEntityNameStrings(trans('permission-manager::base.permission_singular'), trans('permission-manager::base.permission_plural'));
        $this->crud->setRoute(backpack_url('permission'));

        // deny access according to configuration file
        if (config('permission-manager.allow_permission_create') == false) {
            $this->crud->denyAccess('create');
        }
        if (config('permission-manager.allow_permission_update') == false) {
            $this->crud->denyAccess('update');
        }
        if (config('permission-manager.allow_permission_delete') == false) {
            $this->crud->denyAccess('delete');
        }

        if (!Auth::user()->hasRole('superadmin')) {
            $this->crud->denyAccess('list');
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
        }
    }

    public function setupListOperation()
    {
        $this->crud->addColumn([
            'name'  => 'name',
            'label' => trans('permission-manager::base.name'),
            'type'  => 'text',
        ]);

        $this->crud->addColumn([
            'name'  => 'is_for_admins',
            'label' => trans('permission-manager::base.is_for_admins'),
            'type'  => 'check',
        ]);

        if (config('backpack.permissionmanager.multiple_guards')) {
            $this->crud->addColumn([
                'name'  => 'guard_name',
                'label' => trans('permission-manager::base.guard_type'),
                'type'  => 'text',
            ]);
        }
    }

    public function setupCreateOperation()
    {
        $this->addFields();
        $this->crud->setValidation(StoreRequest::class);

        //otherwise, changes won't have effect
        \Cache::forget('spatie.permission.cache');
    }

    public function setupUpdateOperation()
    {
        $this->addFields();
        $this->crud->setValidation(UpdateRequest::class);

        //otherwise, changes won't have effect
        \Cache::forget('spatie.permission.cache');
    }

    private function addFields()
    {
        $this->crud->addField([
            'name'  => 'name',
            'label' => trans('permission-manager::base.name'),
            'type'  => 'text',
        ]);

        $this->crud->addField([
            'name'  => 'is_for_admins',
            'label' => trans('permission-manager::base.is_for_admins'),
            'type'  => 'checkbox',
        ]);

        if (config('backpack.permissionmanager.multiple_guards')) {
            $this->crud->addField([
                'name'    => 'guard_name',
                'label'   => trans('permission-manager::base.guard_type'),
                'type'    => 'select_from_array',
                'options' => $this->getGuardTypes(),
            ]);
        }
    }

    /*
     * Get an array list of all available guard types
     * that have been defined in app/config/auth.php
     *
     * @return array
     **/
    private function getGuardTypes()
    {
        $guards = config('auth.guards');

        $returnable = [];
        foreach ($guards as $key => $details) {
            $returnable[$key] = $key;
        }

        return $returnable;
    }
}
