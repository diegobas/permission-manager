<?php

namespace DiegoBas\PermissionManager\App\Http\Controllers\Admin\Administration;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use DiegoBas\PermissionManager\App\Http\Controllers\Admin\BaseController;
use DiegoBas\PermissionManager\App\Http\Requests\UserStoreCrudRequest as StoreRequest;
use DiegoBas\PermissionManager\App\Http\Requests\UserUpdateCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserCrudController extends BaseController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    private $user;

    public function setup()
    {
        parent::setup();

        $this->user = Auth::user();

        $this->crud->setModel(config('permission-manager.models.user'));
        $this->crud->setEntityNameStrings(trans('permission-manager::base.user'), trans('permission-manager::base.users'));
        $this->crud->setRoute(backpack_url('user'));

        if (!$this->user->hasRole('superadmin')) {
            // Filter users to non administrative
            $userModel = config('permission-manager.models.user');
            if ($userModel != null) {
                $this->crud->query = $userModel::whereHas("roles", function($query) {
                    $query->whereNotIn('name', ['superadmin', 'admin']);
                });
            }

            // Deny permissions to non administrative users
            $current = $this->crud->getCurrentEntry();
            if (is_a($current, $userModel) && $current->hasAnyRole(['superadmin', 'admin'])) {
                $this->crud->denyAccess('update');
                $this->crud->denyAccess('delete');
            }
        }        
    }

    public function setupListOperation()
    {
        $this->crud->setColumns([
            [
                'name'  => 'name',
                'label' => trans('permission-manager::base.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('permission-manager::base.email'),
                'type'  => 'email',
            ],
            [ // n-n relationship (with pivot table)
                'label'     => trans('permission-manager::base.roles'), // Table column heading
                'type'      => 'select_multiple',
                'name'      => 'roles', // the method that defines the relationship in your Model
                'entity'    => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => config('permission.models.role'), // foreign key model
            ],
        ]);

        // Role Filter
        $roles = config('permission.models.role')::whereNotIn('name', ['superadmin', 'admin']);
        if ($this->user->hasRole('superadmin')) {

            $this->crud->addColumn(

                [ // n-n relationship (with pivot table)
                    'label'     => trans('permission-manager::base.extra_permissions'), // Table column heading
                    'type'      => 'select_multiple',
                    'name'      => 'permissions', // the method that defines the relationship in your Model
                    'entity'    => 'permissions', // the method that defines the relationship in your Model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                    'model'     => config('permission.models.permission'), // foreign key model
                ],
            );

            $roles = config('permission.models.role')::all();
        }
        $this->crud->addFilter(
            [
                'name'  => 'role',
                'type'  => 'dropdown',
                'label' => trans('permission-manager::base.role'),
            ],
            $roles->pluck('name', 'id')->toArray(),
            function ($value) { // if the filter is active
                $this->crud->addClause('whereHas', 'roles', function ($query) use ($value) {
                    $query->where('role_id', '=', $value);
                });
            }
        );

        if ($this->user->hasRole('superadmin')) {
            // Extra Permission Filter
            $this->crud->addFilter(
                [
                    'name'  => 'permissions',
                    'type'  => 'select2',
                    'label' => trans('permission-manager::base.extra_permissions'),
                ],
                config('permission.models.permission')::all()->pluck('name', 'id')->toArray(),
                function ($value) { // if the filter is active
                    $this->crud->addClause('whereHas', 'permissions', function ($query) use ($value) {
                        $query->where('permission_id', '=', $value);
                    });
                }
            );
        }
    }

    public function setupCreateOperation()
    {
        $this->addUserFields();
        $this->crud->setValidation(StoreRequest::class);
    }

    public function setupUpdateOperation()
    {
        $this->addUserFields();
        $this->crud->setValidation(UpdateRequest::class);
    }

    /**
     * Store a newly created resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->crud->setRequest($this->crud->validateRequest());
        $this->crud->setRequest($this->handlePasswordInput($this->crud->getRequest()));
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitStore();
    }

    /**
     * Update the specified resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $this->crud->setRequest($this->crud->validateRequest());
        $this->crud->setRequest($this->handlePasswordInput($this->crud->getRequest()));
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitUpdate();
    }

    /**
     * Handle password input fields.
     */
    protected function handlePasswordInput($request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');
        $request->request->remove('roles_show');
        $request->request->remove('permissions_show');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', Hash::make($request->input('password')));
        } else {
            $request->request->remove('password');
        }

        return $request;
    }

    protected function addUserFields()
    {
        $this->crud->addFields([
            [
                'name'  => 'name',
                'label' => trans('permission-manager::base.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('permission-manager::base.email'),
                'type'  => 'email',
            ],
            [
                'name'  => 'password',
                'label' => trans('permission-manager::base.password'),
                'type'  => 'password',
            ],
            [
                'name'  => 'password_confirmation',
                'label' => trans('permission-manager::base.password_confirmation'),
                'type'  => 'password',
            ],            
        ]);

        
        if ($this->user->hasRole('superadmin')) {
            $this->crud->addField(
                [
                    // two interconnected entities
                    'label'             => trans('permission-manager::base.user_role_permission'),
                    'field_unique_name' => 'user_role_permission',
                    'type'              => 'checklist_dependency',
                    'name'              => ['roles', 'permissions'],
                    'subfields'         => [
                        'primary' => [
                            'label'            => trans('permission-manager::base.roles'),
                            'name'             => 'roles', // the method that defines the relationship in your Model
                            'entity'           => 'roles', // the method that defines the relationship in your Model
                            'entity_secondary' => 'permissions', // the method that defines the relationship in your Model
                            'attribute'        => 'name', // foreign key attribute that is shown to user
                            'model'            => config('permission.models.role'), // foreign key model
                            'pivot'            => true, // on create&update, do you need to add/delete pivot table entries?]
                            'number_columns'   => 3, //can be 1,2,3,4,6
                        ],
                        'secondary' => [
                            'label'          => ucfirst(trans('permission-manager::base.permission_singular')),
                            'name'           => 'permissions', // the method that defines the relationship in your Model
                            'entity'         => 'permissions', // the method that defines the relationship in your Model
                            'entity_primary' => 'roles', // the method that defines the relationship in your Model
                            'attribute'      => 'name', // foreign key attribute that is shown to user
                            'model'          => config('permission.models.permission'), // foreign key model
                            'pivot'          => true, // on create&update, do you need to add/delete pivot table entries?]
                            'number_columns' => 3, //can be 1,2,3,4,6
                        ],
                    ],
                ],
            );
        } else {
            $this->crud->addField([
                'label'            => trans('permission-manager::base.roles'),
                'type'             => 'checklist', 
                'name'             => 'roles', // the method that defines the relationship in your Model
                'entity'           => 'roles', // the method that defines the relationship in your Model
                'attribute'        => 'name', // foreign key attribute that is shown to user
                'model'            => config('permission.models.role'), // foreign key model
                'pivot'            => true, // on create&update, do you need to add/delete pivot table entries?]
                'options'   => (function ($query) {
                    return $query->whereNotIn('name', ['superadmin', 'admin'])->pluck('name', 'id')->toArray();
                }),
            ]);
        }        
    }
}
