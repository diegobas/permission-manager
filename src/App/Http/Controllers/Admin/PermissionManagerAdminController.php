<?php

namespace DiegoBas\PermissionManager\App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\AdminController;

class PermissionManagerAdminController extends AdminController {
	
	/**
     * Create a new controller instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
    	return parent::dashboard();
    }

    /**
     * Redirect to the dashboard.
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function redirect()
    {
        return parent::redirect();
    }
}