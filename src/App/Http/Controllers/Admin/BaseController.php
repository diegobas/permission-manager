<?php

namespace DiegoBas\PermissionManager\App\Http\Controllers\Admin;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

use App;

class BaseController extends CrudController {

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $usesTranslatable = in_array('Translatable', class_uses($this));
        if ($usesTranslatable) {
            $locales = config('backpack.crud.locales');
            $this->crud->addFilter(
                [
                    'type' => 'dropdown',
                    'name' => 'locale',
                    'label' => trans('permission-manager::base.language'),
                ],
                function() use ($locales) {
                    foreach ($locales as $key => $value) {
                        $locales[$key] = trans('permission-manager::base.lang_'.$key);
                    }
                    
                    return $locales;
                },
                function ($value) {
                    $this->setLocale($value);
                }
            );
        }
    }

    protected function setupListOperation()
    {
        
    }

    protected function setupCreateOperation()
    {
        
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    // CUstom methods

    public function setLocale($locale = 'es')
    {
        config(['app.locale' => $locale]);
        App::setLocale($locale);
    }
}