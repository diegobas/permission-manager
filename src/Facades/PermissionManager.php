<?php

namespace DiegoBas\PermissionManager\Facades;

use Illuminate\Support\Facades\Facade;

class PermissionManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'permission-manager';
        //return 'PermissionManager';
    }
}
