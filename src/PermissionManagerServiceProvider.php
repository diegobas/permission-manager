<?php

namespace DiegoBas\PermissionManager;

use Illuminate\Support\ServiceProvider;

use DiegoBas\PermissionManager\App\Console\Commands\Install as PermissionManagerInstall;
use DiegoBas\PermissionManager\App\Console\Commands\Admin as PermissionManagerAdmin;
use DiegoBas\PermissionManager\App\Models\Permission;
use DiegoBas\PermissionManager\App\Models\Role;
use DiegoBas\PermissionManager\App\Http\Observers\RoleObserver;

use Illuminate\Support\Facades\Session;

class PermissionManagerServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // Override backpack views configuration);
        $config = [];
        $config['backpack.crud.locales'] = [
            "en" => "English",
            "es" => "Spanish",
        ];
        $config['setup_dashboard_routes'] = false;
        
        config($config);

        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'permission-manager');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'permission-manager');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        
        $this->setupRoutes();

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

        Permission::created(function($permission) {
            $superadmin = Role::findByName('superadmin');
            if ($superadmin) {
                $superadmin->givePermissionTo($permission);
            }
        });

        /*
        Role::deleting(function($role) {
            if ($role->name == 'superadmin') {
                return false;
            }
        });
        */

        Role::observe(RoleObserver::class);

    }

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    private function setupRoutes()
    {
        $webRouteFilePath = __DIR__.'/../routes/web.php';
        $adminRouteFilePath = __DIR__.'/../routes/admin.php';

        $this->loadRoutesFrom($webRouteFilePath);
        $this->loadRoutesFrom($adminRouteFilePath);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/permission-manager.php', 'permission-manager');

        // Register the service the package provides.
        $this->app->singleton('permission-manager', function ($app) {
            return new PermissionManager;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['permission-manager'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/permission-manager.php' => config_path('permission-manager.php'),
        ], 'permission-manager.config');

        // Publishing the views.
        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/diegobas'),
        ], 'permission-manager.views');

        // Publishing assets.
        $this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/diegobas'),
        ], 'permission-manager.views');

        // Publishing the translation files.
        $this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/diegobas'),
        ], 'permission-manager.views');

        // Registering package commands.
        if ($this->app->runningInConsole()) {
            $this->commands([
                PermissionManagerInstall::class,
                PermissionManagerAdmin::class,
            ]);
        }
    }
}
