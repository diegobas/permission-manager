<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Common 
    |--------------------------------------------------------------------------
    */
    'all'           => 'Mostrar todos',
    'title'         => 'Título',
    'subtitle'      => 'Subtítulo',
    'content'       => 'Contenido',
    'category'      => 'Categoría',
    'categories'    => 'Categorías',
    'groups'        => 'Grupo|Grupos',
    'image'         => 'Imagen',
    'active'        => 'Activo',
    'activate'      => 'Activar',
    'deleted'       => 'Eliminado',
    'restore'       => 'Restaurar',
    'published'     => 'Publicado',
    'publis_at'     => 'Publicado en',
    'unpublish_at'  => 'Despublicado en',
    'starts_at'     => 'Comienza en',
    'ends_at'       => 'Finaliza en',
    'language'      => 'Idioma',
    'close'         => 'Cerrar',
    'save'          => 'Guardar',
    'send'          => 'Enviar',

    'lang_es'       => 'Español',
    'lang_va'       => 'Valenciano',
    'lang_en'       => 'Inglés',

    'menu_list'         => 'Entradas',
    'menu_categories'   => 'Categorías',

    'alert_restore_title'   => '¿Está Vd. seguro?',
    'alert_restore_content' => 'Esta operación restaurará también las relaciones en los registros correspondientes.',

    'restore_confirmation_not_title'    => 'No se pudo restaurar',
    'restore_confirmation_not_message'  => 'Ha ocurrido un error. Puede que el elemento no haya sido restaurado.',

    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    */
    'administration'        => 'Administración',
    'name'                  => 'Nombre',
    'role'                  => 'Rol',
    'roles'                 => 'Roles',
    'roles_have_permission' => 'Roles con este permiso',
    'permission_singular'   => 'Permiso',
    'permission_plural'     => 'Permisos',
    'permission'            => 'Permiso',
    'permissions'           => 'Permisos',
    'is_for_admins'         => '¿Es para Admins.?',
    'can_list'              => 'Lista',
    'can_view'              => 'Ver',
    'can_create'            => 'Crear',
    'can_update'            => 'Modificar',
    'can_delete'            => 'Eliminar',
    'user_singular'         => 'Usuario',
    'user_plural'           => 'Usuarios',
    'email'                 => 'Correo electrónico',
    'extra_permissions'     => 'Permisos adicionales',
    'password'              => 'Contraseña',
    'password_confirmation' => 'Confirmación de contraseña',
    'user_role_permission'  => 'Permisos del rol del usuario',
    'user'                  => 'Usuario',
    'users'                 => 'Usuarios',

];