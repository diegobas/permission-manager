<?php

return [  
    /*
    |--------------------------------------------------------------------------
    | Common 
    |--------------------------------------------------------------------------
    */
    'all'           => 'Show all',
    'title'         => 'Title',
    'subtitle'      => 'Subtitle',
    'content'       => 'Content',
    'category'      => 'Category',
    'categories'    => 'Categories',
    'groups'        => 'Group|Groups',
    'image'         => 'Image',
    'active'        => 'Active',
    'activate'      => 'Activate',
    'deleted'       => 'Deleted',
    'restore'       => 'Restore',
    'published'     => 'Published',
    'publish_at'    => 'Publish at',
    'unpublish_at'  => 'Unpublish at',
    'starts_at'     => 'Starts at',
    'ends_at'       => 'Ends at',
    'language'      => 'Language',
    'close'         => 'Close',
    'save'          => 'Save',
    'send'          => 'Send',

    'lang_es'       => 'Spanish',
    'lang_va'       => 'Valencian',
    'lang_en'       => 'English',

    'menu_list'         => 'Entries',
    'menu_categories'   => 'Categories',

    'alert_restore_title'   => '¿Are you sure?',
    'alert_restore_content' => 'This operation restores the relationships of recovered register.',

    'restore_confirmation_not_title'    => 'Not restored',
    'restore_confirmation_not_message'  => "There's been an error. Your item might not have been restored.",

    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    */
    'administration'        => 'Administration',
    'name'                  => 'Name',
    'role'                  => 'Role',
    'roles'                 => 'Roles',
    'roles_have_permission' => 'Roles that have this permission',
    'permission_singular'   => 'permission',
    'permission_plural'     => 'permissions',
    'permission'            => 'Permission',
    'permissions'           => 'Permissions',
    'is_for_admins'         => 'Is for Admins?',
    'can_list'              => 'List',
    'can_view'              => 'View',
    'can_create'            => 'Create',
    'can_update'            => 'Update',
    'can_delete'            => 'Delete',
    'user_singular'         => 'User',
    'user_plural'           => 'Users',
    'email'                 => 'Email',
    'extra_permissions'     => 'Extra Permissions',
    'password'              => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'user_role_permission'  => 'User Role Permissions',
    'user'                  => 'User',
    'users'                 => 'Users',

];