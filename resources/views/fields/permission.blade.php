<!-- checklist -->
@php
  $model = new $field['model'];
  $key_attribute = $model->getKeyName();
  $identifiable_attribute = $field['attribute'];

  // calculate the checklist options
  $field['options'] = $field['model']::all()->pluck($identifiable_attribute, $key_attribute)->toArray();
  foreach($field['options'] as $key => $value) {
    $field['options'][$key] = [
      'name'  => $value,
      'can_list' => 0,
      'can_view' => 0,
      'can_create' => 0,
      'can_update' => 0,
      'can_delete' => 0
    ];
  }

  // calculate the value of the hidden input
  $field['value'] = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? [];
  if ($field['value'] instanceof Illuminate\Database\Eloquent\Collection) {
    $values = [];
    foreach($field['value'] as $permission) {
      $values[$permission[$key_attribute]] = [    
          'can_list' => $permission->pivot->can_list,
          'can_view' => $permission->pivot->can_view,
          'can_create' => $permission->pivot->can_create,
          'can_update' => $permission->pivot->can_update,
          'can_delete' => $permission->pivot->can_delete,       
      ];

      if (isset($field['options'][$key_attribute])) {
        $field['options'][$key_attribute]['can_list'] = $permission->pivot->can_list;
        $field['options'][$key_attribute]['can_view'] = $permission->pivot->can_view;
        $field['options'][$key_attribute]['can_create'] = $permission->pivot->can_create;
        $field['options'][$key_attribute]['can_update'] = $permission->pivot->can_update;
        $field['options'][$key_attribute]['can_delete'] = $permission->pivot->can_delete;
      }
    }

    $field['value'] = $values;
  } elseif (is_string($field['value'])){
    $field['value'] = json_decode($field['value']);
  }

  // define the init-function on the wrapper
  $field['wrapper']['data-init-function'] =  $field['wrapper']['data-init-function'] ?? 'bpFieldInitChecklist';
@endphp

@include('crud::fields.inc.wrapper_start')
    <label>{!! $field['label'] !!}</label>
    @include('crud::fields.inc.translatable_icon')

    <input type="hidden" value='@json($field['value'])' name="{{ $field['name'] }}">

    @foreach ($field['options'] as $key => $option)
          <div class="row">
            <div class="permission col-sm-5">
              <input id="role_permission" type="checkbox" value="{{ $key }}">
              <label class="font-weight-normal">{{ $option['name'] }}</label>
            </div>
            <div class="role_permission_can col-sm row" data-id="{{ $key }}">
            @php
            $permission_values = array_filter($option, function($key) {
              return $key !== 'name';
            }, ARRAY_FILTER_USE_KEY);
            @endphp
            @foreach ($permission_values as $permission => $value)
              <div class="col-sm">
                <div class="checkbox">
                  <input id="role_permission_can" type="checkbox" value="{{ $permission }}">
                  <label class="font-weight-normal">{{ trans('permission-manager::base.' . $permission) }}</label>
                </div>
              </div>
            @endforeach
            @php
            $field['options'][$key] = $permission_values;
            @endphp
            </div>
          </div>
    @endforeach

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
@include('crud::fields.inc.wrapper_end')


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp
    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        <script>
            var all_options = JSON.parse('@json($field['options'])');
            var selected_options = JSON.parse('@json($field['value'])');
            var newValue = [];
                
            function bpFieldInitChecklist(element) {
                var hidden_input = element.find('input[type=hidden]');
                var permissions = element.find('input[type=checkbox]#role_permission');
                var container = element.find('.row');                
                  
                permissions.each(function(key, option) {
                  var id = $(this).val();
                  var option = all_options[id];
                  var newLine = {};
                  Object.defineProperty(newLine, id.toString(), {
                    value: option,
                    writable: true,
                    enumerable: true,
                    configurable: true
                  });

                  var all_link = $(this).parent().find('a#role_permission_all');
                  var can_checkboxes = element.find('div[data-id='+id+'].role_permission_can input[type=checkbox]#role_permission_can');
                  if (selected_options[id] !== undefined) {
                    $(this).prop('checked', 'checked');
                    $(this).next().contents().wrap('<a href="#" onclick="select_all('+id+')"></a>');
                    can_checkboxes.prop('disabled', false);
                    newValue.push(newLine);
                  } else {
                    $(this).prop('checked', false);
                    can_checkboxes.prop('disabled', 'disabled');                    
                  }

                  $(this).click(function() {
                    if ($(this).is(':checked')) {
                      newValue.push(newLine);
                      can_checkboxes.prop('disabled', false);
                      $(this).next().contents().wrap('<a href="#" onclick="select_all('+id+')"></a>');
                    } else {
                      var index = newValue.indexOf(newLine);
                      if (index >= 0) {
                        newValue.splice(index, 1);
                      }
                      can_checkboxes.prop('disabled', 'disabled');
                      $(this).next().find('a').contents().unwrap();
                    }
                    hidden_input.val(JSON.stringify(newValue));
                  });
                });

                var permission_checkboxes = element.find('input[type=checkbox]#role_permission_can');
                permission_checkboxes.each(function(key, option) {
                  var id = $(this).parents('div.role_permission_can').attr('data-id');

                  if (selected_options[id] !== undefined && selected_options[id][this.value] !== undefined && selected_options[id][this.value] === 1) {
                    $(this).prop('checked', 'checked');
                  } else {
                    $(this).prop('checked', false);
                  }
                  update_selected(hidden_input, this, id, $(this).is(':checked') ? 1 : 0);

                  $(this).click(function() {
                    var value = 0;
                    if ($(this).is(':checked')) {
                      value = 1;
                    }

                    update_selected(hidden_input, this, id, value);
                  });
                });
            }

            function select_all(id) {
              var hidden_input = $('input[type=hidden][name={{ $field['name'] }}]');
              var checkboxes = $('div[data-id='+id+'].role_permission_can').find('input[type=checkbox]#role_permission_can');
              checkboxes.prop('checked', 'checked');

              if (hidden_input !== undefined) {
                checkboxes.each(function() {
                  update_selected(hidden_input, this, id, 1);
                });
              }
            }

            function update_selected(hidden_input, checkbox, id, value) {
              var newLine = {};
              Object.defineProperty(newLine, id.toString(), {
                value: selected_options[id],
                writable: true,
                enumerable: true,
                configurable: true
              });
              var idx = newValue.findIndex(function(element, index) {
                return Object.keys(element)[0] == Object.keys(newLine)[0];
              });

              all_options[id][checkbox.value] = value;
              if (selected_options[id] !== undefined) {
                selected_options[id][checkbox.value] = value;
              }
              
              if (idx >= 0) {
                newValue[idx][id][checkbox.value] = value;

                hidden_input.val(JSON.stringify(newValue));
              }
            }
        </script>
    @endpush

@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
