<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item">
	<a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a>
</li>

@canany(['users', 'user_groups', 'user_permissions'])
<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-globe"></i> {{ trans('permission-manager::base.administration') }}</a>

	<ul class="nav-dropdown-items">
		@can('users')
		<li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon fa fa-list"></i> <span>{{ trans('permission-manager::base.users') }}</span></a></li>
		@endcan
		@can('user_groups')
		<li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon fa fa-cubes"></i> <span>{{ trans('permission-manager::base.roles') }}</span></a></li>
		@endcan
		@can('user_permissions')
		<li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon fa fa-cubes"></i> <span>{{ trans('permission-manager::base.permissions') }}</span></a></li>
		@endcan
	</ul>
</li>
@endcan