<!-- This file is used to store topbar (right) items -->
{{-- <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="fa fa-bell"></i><span class="badge badge-pill badge-danger">5</span></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="fa fa-list"></i></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="fa fa-map"></i></a></li> --}}

@php $locale = session('locale', config('app.locale')); @endphp
<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <img src="{{asset('vendor/diegobas/img/'.$locale.'.png')}}" width="20px"> {{ trans('permission-manager::base.lang_' . $locale) }}
       <span class="caret"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        @foreach (config('backpack.crud.locales') as $key => $locale)
        <a class="dropdown-item" href="{{ route('lang', ['locale' => $key]) }}"><img src="{{asset('vendor/diegobas/img/'.$key.'.png')}}" width="20px"> {{ trans('permission-manager::base.lang_' . $key) }}</a>
        @endforeach
    </div>
</li>