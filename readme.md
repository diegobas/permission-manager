# PermissionManager

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]

This is where your description should go. Take a look at [contributing.md](contributing.md) to see a to do list.

## Installation

Via Composer

```bash
$ composer require diegobas/permission-manager:dev-master
```

## Usage

First, edit your **.env** file with your own data.

Second, install Backpack for Laravel

```bash
$ php artisan backpack:install
```

Change the user model in **auth.php** config file with:

```php
'model' => DiegoBas\PermissionManager\App\Models\User::class,
```

and uncomment next middleware in **backpack\base.php** config file:

```php
\Backpack\CRUD\app\Http\Middleware\UseBackpackAuthGuardInsteadOfDefaultAuthGuard::class,
```

Third, install PermissionManager.

```bash
$ php artisan permission-manager:install
```

If you want to create a new user, write the next sentence and follow the instructions:

```bash
$ php artisan backpack:admin --option=user
```

If you want to assign a role to a user:

```bash
$ php artisan backpack:admin --option=role
```

Only the **superadmin** role is created by default. Use it to allow specific user to access via web with permissions to create new users and roles.

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email author email instead of using the issue tracker.

## Credits

- [Diego Bas][link-author]
- [All Contributors][link-contributors]

## License

license. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/diegobas/permission-manager.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/diegobas/permission-manager.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/diegobas/permission-manager
[link-downloads]: https://packagist.org/packages/diegobas/permission-manager
[link-author]: https://github.com/diegobas
[link-contributors]: ../../contributors
